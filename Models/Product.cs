namespace ProductAPI.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string description { get; set; }
        public int ratings { get; set; }
    }
}